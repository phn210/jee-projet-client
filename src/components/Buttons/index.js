import { Button } from "@mui/material";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { signOut } from "../../redux/userDataSlice";

export function SigninButton() {
  return (
    <NavLink
      to="/auth"
    >
      <Button
        color="primary"
        variant="contained"
        style={{ minWidth: "150px", marginRight: "0rem" }}
      >
        Sign In
      </Button>
    </NavLink>
  );
}

export function SignoutButton() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  return (
    <Button
      color="primary"
      variant="outlined"
      style={{ minWidth: "150px", marginRight: "0rem" }}
      onClick={() => {
        dispatch(signOut());
        navigate("/auth");
      }}
    >
      Sign Out
    </Button>
  );
}