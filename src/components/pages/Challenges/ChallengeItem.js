import { Box, Paper, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import { BorderCircle, ErrorIcon, SuccessIcon } from "../../Icon";


export default function ChallengeItem({ challengeData }) {
  const theme = useTheme();
  const { username } = useSelector((state) => state.userDataSlice);
  return (
    <NavLink
      to={`/challenges/${challengeData.challengeId}`}
      style={{
        width: "100%",
        textDecoration: "none",
        "&:hover": {
          textDecoration: "none",
        },
      }}
    >
      <Paper
        style={{
          padding: "0.5rem 1rem",
          margin: "0.5rem 0.5rem",
          backgroundColor: "#fffcf9"
        }}
      >
        <Box display="flex" flexDirection="column" m={2}>
          <Box mb={1}>
            <Box display="flex" flexDirection="row"
              justifyContent="space-between"
            >
              <Typography fontSize="25px" fontWeight="500" >{challengeData.name}</Typography>
              {challengeData.submisisons && challengeData.submisisons.length > 0 &&
                challengeData.submisisons.map(e => e.username).includes(username) &&
                (challengeData.submisisons.filter(e => e.username === username)[0]?.isCorrect ?
                  <SuccessIcon style={{
                    width: 30,
                    fontSize: "40px",
                    color: theme.palette.text.secondary,
                  }} /> :
                  <ErrorIcon style={{
                    width: 30,
                    fontSize: "40px",
                    color: theme.palette.text.secondary,
                  }} />
                )
              }
              {(!challengeData.submisisons || challengeData.submisisons.length == 0 ||
                !challengeData.submisisons.map(e => e.username).includes(username)) &&
                <BorderCircle style={{
                  width: 30,
                  fontSize: "40px",
                  color: theme.palette.text.secondary,
                }} />}
            </Box>
          </Box>
          <Box mb={2}>
            <Typography fontSize="16px" fontWeight="300" >
              {challengeData.submisisons ? challengeData.submisisons.length : 0} submissions
            </Typography>
          </Box>
          <Box mb={1}>
            <Typography variant="p">{
              (challengeData.description && challengeData.description.length > 250)
                ? challengeData.description.slice(0, 250).trim() + "..."
                : challengeData.description
            }</Typography>
          </Box>
        </Box>
      </Paper >
    </NavLink>
  );
}