import { Box } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";
import { fetchAllChallengesData } from "../../../redux/publicDataSlice";
import { QUERY_STATE } from "../../../configs/constance";
import Empty from "../../Icon/Empty";
import ChallengeItem from "./ChallengeItem";

export function ChallengleList() {
  const dispatch = useDispatch();
  const { fetchingAllChallengesStatus, challenges } = useSelector((state) => state.publicDataSlice);

  useEffect(() => {
    if (fetchingAllChallengesStatus === QUERY_STATE.INITIAL) dispatch(fetchAllChallengesData());
    console.log("fetch data");
  }, [fetchingAllChallengesStatus, challenges]);

  return (
    <Box
      display="flex" justifyContent="start"
      flexDirection="column"
      padding={2}
    >
      {(challenges && challenges.length) ?
        // <>
        challenges.map(data => <ChallengeItem challengeData={data} />)
        // </>
        : <Empty title="There are no CTFs" iconProps={{ style: { fontSize: "60px" } }} />
      }
    </Box >
  );
}

export default function ChallengesPage() {
  return (
    <PageLayoutWrapper name="Challenges">
      <ChallengleList />
    </PageLayoutWrapper>
  );
}