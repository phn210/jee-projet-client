import { Box, Button, Paper, TextField, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { useEffect } from "react";
import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";
import { fetchChallengeDetailData } from "../../../redux/publicDataSlice";
import { QUERY_STATE } from "../../../configs/constance";
import { BorderCircle, ErrorIcon, SuccessIcon } from "../../Icon";

export function ChallengeDetails() {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { username } = useSelector((state) => state.userDataSlice);
  const { fetchingChallengeDetailStatus, challengeDetail } = useSelector((state) => state.publicDataSlice);
  const location = useLocation();

  useEffect(() => {
    const challengeId = Number(location.pathname.split("/").slice(-1)[0]);
    if (!isNaN(challengeId) &&
      (
        (challengeDetail && challengeDetail.ctfId !== challengeId)
        || fetchingChallengeDetailStatus === QUERY_STATE.INITIAL
      )
    )
      dispatch(fetchChallengeDetailData(challengeId));
  }, [fetchingChallengeDetailStatus, challengeDetail]);

  return (
    <>
      {challengeDetail && Object.keys(challengeDetail).length > 0 && (
        <Box display="flex" flexDirection="column" m={2}>
          <Box display="flex" flexDirection="row"
            style={{
              margin: "0.5rem 0.5rem",
            }}
          >
          </Box>
          <Paper
            style={{
              padding: "0.5rem 1rem",
              margin: "0.5rem 0.5rem",
              backgroundColor: "#fffcf9"
            }}
          >
            <Box display="flex" flexDirection="column" m={2}>
              <Box mb={1}>
                <Box display="flex" flexDirection="row"
                  justifyContent="space-between"
                >
                  <Typography fontSize="25px" fontWeight="500" >{challengeDetail.name}</Typography>
                  {challengeDetail.submisisons && challengeDetail.submisisons.length > 0 &&
                    challengeDetail.submisisons.map(e => e.username).includes(username) &&
                    (challengeDetail.submisisons.filter(e => e.username === username)[0]?.isCorrect ?
                      <SuccessIcon style={{
                        width: 30,
                        fontSize: "40px",
                        color: theme.palette.text.secondary,
                      }} /> :
                      <ErrorIcon style={{
                        width: 30,
                        fontSize: "40px",
                        color: theme.palette.text.secondary,
                      }} />
                    )
                  }
                  {(!challengeDetail.submisisons || challengeDetail.submisisons.length == 0 ||
                    !challengeDetail.submisisons.map(e => e.username).includes(username)) &&
                    <BorderCircle style={{
                      width: 30,
                      fontSize: "40px",
                      color: theme.palette.text.secondary,
                    }} />}
                </Box>
              </Box>
              <Box mb={2}>
                <Typography fontSize="16px" fontWeight="300" >
                  {challengeDetail.submisisons ? challengeDetail.submisisons.length : 0} submissions
                </Typography>
              </Box>

              <Box mb={1}>
                <Typography variant="p">{
                  (challengeDetail.description && challengeDetail.description.length > 250)
                    ? challengeDetail.description.slice(0, 250).trim() + "..."
                    : challengeDetail.description
                }</Typography>
              </Box>
            </Box>
          </Paper>
          <Paper
            style={{
              padding: "0.5rem 1rem",
              margin: "0.5rem 0.5rem",
              backgroundColor: "#fffcf9"
            }}
          >
            <Box display="flex" flexDirection="column" m={2}>
              <Box mb={1}>
                <Typography fontSize="25px" fontWeight="500" >Details</Typography>
              </Box>
              <Box mb={2}>
                {challengeDetail.details.split("\n").map(e => (
                  <Box mb={1}>
                    <Typography variant="p">{e}</Typography>
                  </Box>
                ))}
              </Box>
            </Box>
          </Paper>
          <Paper
            style={{
              padding: "0.5rem 1rem",
              margin: "0.5rem 0.5rem",
              backgroundColor: "#fffcf9"
            }}
          >
            <Box display="flex" flexDirection="column" m={2}>
              <Box mb={1} display="flex" flexDirection="row" justifyContent="space-between">
                <Typography fontSize="25px" fontWeight="500" >Submit Result</Typography>
                {!challengeDetail.result && <Button
                  variant="outlined" color="primary"
                  onClick={() => { }}>
                  Submit
                </Button>
                }
              </Box>

              <Box mb={1}>
                <TextField
                  id="submission"
                  // label="Comment"
                  value={challengeDetail.result}
                  onChange={() => { }}
                  fullWidth />
              </Box>
            </Box>
          </Paper >
        </Box >
      )
      }
    </>

  );
}

export default function ChallengeDetailsPage() {

  return (
    <PageLayoutWrapper name="Challenge Details">
      <ChallengeDetails />
    </PageLayoutWrapper>
  );
}