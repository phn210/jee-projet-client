import { Box, Button, Paper, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";
import { MemberList } from "./MemberList";
import { fetchAllTeamsData } from "../../../redux/publicDataSlice";
import { QUERY_STATE } from "../../../configs/constance";
import Empty from "../../Icon/Empty";

export function YourTeam({ team }) {
  const { username } = useSelector((state) => state.userDataSlice);

  return (
    <Paper
      style={{
        padding: "1rem 2rem",
        margin: "2rem 2rem",
        backgroundColor: "#fffcf9"
      }}
    >
      <Box display="flex" flexDirection="row" justifyContent="space-between" m={2}>
        {/* <Box display="flex" flexDirection="column" alignItems="start" m={1}> */}
        <Typography variant="h5" mb={1}>{team.name}</Typography>
        {/* </Box> */}
        {username === team.owner ? (
          <Button
            variant="outlined"
            color="error"
            onClick={(e) => { }}>
            Delete Team
          </Button>
        ) : (
          <Button
            variant="outlined"
            color="error"
            onClick={(e) => { }}>
            Leave Team
          </Button>
        )}
      </Box>
      <MemberList owner={team.owner} members={team.members} currentUser={username} />
    </Paper>
  )
}

export function TeamList({ teams }) {
  const [newTeam, setNewTeam] = useState("");

  const handleUpdateTeamName = (e) => {
    setNewTeam(e.target.value);
  }

  const handleCreateTeam = () => {
    console.log(newTeam);
  }

  return (
    <Box
      display="flex" justifyContent="start"
      flexDirection="column"
      padding={2}
    >
      <Box sx={{ width: "100%", margin: "1rem" }}
        display="flex" flexDirection="row" justifyContent="start"

      >
        <TextField
          id="team_name"
          label="Team Name"
          value={newTeam}
          onChange={(e) => handleUpdateTeamName(e)}
        />
        <Button
          variant="outlined"
          color="primary"
          style={{
            margin: "0rem 1rem",
          }}
          onClick={(e) => handleCreateTeam()}>
          Create a team
        </Button>
      </Box>
      {(teams && teams.length) ?
        teams.map(data => (
          <>
            <Paper
              style={{
                padding: "1rem 2rem",
                margin: "1rem 1rem",
                backgroundColor: "#fffcf9"
              }}
            >
              <Box sx={{ width: "100%" }}
                display="flex" flexDirection="row" justifyContent="space-between"
              >
                <Typography fontSize="20px" fontWeight="400">{data.name}</Typography>
                <Typography>Owner: {data.owner}</Typography>
                <Typography>{data.members.length} members</Typography>
                <Button
                  variant="outlined"
                  color="success"
                  onClick={(e) => { }}>
                  Join Team
                </Button>
              </Box>
            </Paper>
          </>
        ))
        : <Empty title="There are no teams" iconProps={{ style: { fontSize: "60px" } }} />
      }
    </Box >
  )
}

export function Team() {
  const dispatch = useDispatch();
  const { participant } = useSelector((state) => state.userDataSlice);
  const { fetchingAllTeamsStatus, teams } = useSelector((state) => state.publicDataSlice);

  useEffect(() => {
    if (fetchingAllTeamsStatus === QUERY_STATE.INITIAL) dispatch(fetchAllTeamsData());
  }, [fetchingAllTeamsStatus, teams]);

  return (
    <Box>
      {
        participant.team.teamId ?
          <YourTeam team={participant.team} /> :
          <TeamList teams={teams} />
      }
    </Box>
  );
}

export default function TeamPage() {
  return (
    <PageLayoutWrapper name="Team">
      <Team />
    </PageLayoutWrapper>
  );
}