import {
  Avatar, Box, Button, Divider, List, ListItem,
  ListItemAvatar, Typography
} from "@mui/material";
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import { current } from "@reduxjs/toolkit";


export function MemberList({ owner, members, currentUser = "" }) {
  return (
    <Box>
      <List sx={{ width: '100%' }}>
        <Divider mb={1} />
        {members && members.length > 0 && (
          members.map(member => (
            <>
              <ListItem alignItems="center">
                <ListItemAvatar>
                  <Avatar alt={member} src="./" />
                </ListItemAvatar>
                <Box sx={{ width: "100%" }}
                  display="flex" flexDirection="row" justifyContent="space-between"
                >
                  <Typography>{member}</Typography>
                  {owner && owner === currentUser && (
                    !(member === currentUser) && <Button onClick={(e) => { }} >
                      <DeleteOutlineOutlinedIcon color="error" />
                    </Button>
                  )}
                </Box>
              </ListItem>
              <Divider mb={1} />
            </>
          ))
        )}
      </List>
    </Box>
  )
}