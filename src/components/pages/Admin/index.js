import { Box, Button, Paper, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";
import { fetchPendingCTFsData } from "../../../redux/publicDataSlice";
import { QUERY_STATE } from "../../../configs/constance";
import CTFItem from "../CTFs/CTFItem";
import Empty from "../../Icon/Empty";

export function CTFList({ onUpdateView }) {
  const dispatch = useDispatch();
  const { fetchingPendingCTFsStatus, pendingCtfs } = useSelector((state) => state.userDataSlice);

  useEffect(() => {
    if (fetchingPendingCTFsStatus === QUERY_STATE.INITIAL) dispatch(fetchPendingCTFsData());
  }, [fetchingPendingCTFsStatus, pendingCtfs]);

  return (
    <Box
      display="flex" justifyContent="start"
      flexDirection="column"
    >
      {/* <Box m={1}> */}
      {/* <Button
          variant="contained" fullWidth
          color="primary"
          // style={{ margin: "0 0.5rem 0 0.5rem" }}
          onClick={(e) => onUpdateView(e, 3)}>
          New CTF
        </Button> */}
      {/* </Box> */}
      {console.log(pendingCtfs)}
      {(pendingCtfs && pendingCtfs.length) ?
        pendingCtfs.map(
          data => <CTFItem ctfData={data} isOrganizer={true} />
        )
        : <Empty title="There are no CTFs" iconProps={{ style: { fontSize: "60px" } }} />
      }
    </Box >
  );
}

export function UserList({ onUpdateView }) {
  return (<>User List</>);
}

export function Admin({ viewIndex }) {
  const [view, setView] = useState(0);

  useEffect(() => {
    setView(viewIndex || 1);
  }, []);

  const handleUpdateView = (e, viewIndex) => {
    setView(viewIndex);
  }
  return (
    <Box display="flex" justifyContent="start"
      flexDirection="column"
      padding={2}>
      <Box mb={2} display="flex" justifyContent="space-between" flexDirection="row">
        <Button
          variant="outlined" fullWidth
          color="primary" style={{ margin: "0 0.5rem 0 0.5rem" }}
          onClick={(e) => handleUpdateView(e, 0)}>
          Pending CTFs
        </Button>
        <Button
          variant="outlined" fullWidth
          color="primary" style={{ margin: "0 0.5rem 0 0.5rem" }}
          onClick={(e) => handleUpdateView(e, 2)}>
          Users
        </Button>
      </Box>
      {view === 1 && <CTFList onUpdateView={handleUpdateView} />}
      {view === 2 && <UserList onUpdateView={handleUpdateView} />}
    </Box>
  );
}

export default function AdminPage() {
  return (
    <PageLayoutWrapper name="Admin">
      <Admin />
    </PageLayoutWrapper>
  );
}