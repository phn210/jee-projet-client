import { Box } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";
import { fetchAllCTFsData } from "../../../redux/publicDataSlice";
import { QUERY_STATE } from "../../../configs/constance";
import Empty from "../../Icon/Empty";
import CTFItem from "./CTFItem";

export function CTFList() {
  const dispatch = useDispatch();
  const { fetchingAllCTFsStatus, ctfs } = useSelector((state) => state.publicDataSlice);

  useEffect(() => {
    if (fetchingAllCTFsStatus === QUERY_STATE.INITIAL) dispatch(fetchAllCTFsData());
    console.log("fetch data");
  }, [fetchingAllCTFsStatus, ctfs]);

  return (
    <Box
      display="flex" justifyContent="start"
      flexDirection="column"
      padding={2}
    >
      {(ctfs && ctfs.length) ?
        // <>
        ctfs.map(data => <CTFItem ctfData={data} />)
        // </>
        : <Empty title="There are no CTFs" iconProps={{ style: { fontSize: "60px" } }} />
      }
    </Box >
  );
}

export default function CTFsPage() {
  return (
    <PageLayoutWrapper name="CTFs">
      <CTFList />
    </PageLayoutWrapper>
  );
}