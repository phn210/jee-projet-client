import { Box, Button, Paper, TextField, Typography } from "@mui/material";
import { useEffect } from "react";
import { useLocation, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";
import { fetchCTFDetailData } from "../../../redux/publicDataSlice";
import { QUERY_STATE, ROLE } from "../../../configs/constance";
import { CommentList } from "./Comments";
import { participateIndividual } from "../../../redux/userDataSlice";

export function CTFDetails() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { role, team, username } = useSelector((state) => state.userDataSlice);
  const { fetchingCTFDetailStatus, ctfDetail } = useSelector((state) => state.publicDataSlice);
  const location = useLocation();

  useEffect(() => {
    const ctfId = Number(location.pathname.split("/").slice(-1)[0]);
    if (!isNaN(ctfId) &&
      (
        (ctfDetail && ctfDetail.ctfId !== ctfId)
        || fetchingCTFDetailStatus === QUERY_STATE.INITIAL
      )
    )
      dispatch(fetchCTFDetailData(ctfId));
  }, [fetchingCTFDetailStatus, ctfDetail]);

  const handleParticipateIndividual = () => {
    dispatch(participateIndividual(ctfDetail.ctfId));
    navigate(0);
  }
  const handleParticipateTeam = () => { }

  return (
    <>
      {ctfDetail && Object.keys(ctfDetail).length > 0 && (
        <Box display="flex" flexDirection="column" m={2}>
          <Box display="flex" flexDirection="row"
            style={{
              margin: "0.5rem 0.5rem",
            }}
          >
            {team && team.isTeamOwner && (
              <Button
                variant="contained" color="primary" style={{ margin: "0 1rem 0 0" }}
                onClick={() => { }}>
                Register your Team
              </Button>
            )}
            {role === ROLE.PARTICIPANT && (
              ctfDetail.participants.map(p => p.userId.username).includes(username)
                ? <Button
                  variant="outlined" color="primary"
                  disabled>
                  Participated
                </Button>
                : <Button
                  variant="contained" color="primary"
                  onClick={() => handleParticipateIndividual()}>
                  Participate
                </Button>
            )}
          </Box>
          <Paper
            style={{
              padding: "0.5rem 1rem",
              margin: "0.5rem 0.5rem",
              backgroundColor: "#fffcf9"
            }}
          >
            <Box display="flex" flexDirection="column" m={2}>
              <Box mb={1}>
                <Box display="flex" flexDirection="row"
                  justifyContent="space-between"
                >
                  <Typography fontSize="25px" fontWeight="500" >{ctfDetail.name}</Typography>
                </Box>
              </Box>
              <Box mb={2}>
                <Typography fontStyle="italic" mb={1}>
                  {ctfDetail.organizer ? ctfDetail.organizer.name : "Error"}
                  &nbsp;-&nbsp;
                  {ctfDetail.organizer ? ctfDetail.organizer.contact : "Error"}
                </Typography>
                <Typography ontSize="16px" fontWeight="300" mb={1}>
                  {ctfDetail.participants ? ctfDetail.participants.length : 0} participants, {ctfDetail.numViews || 0} views, {ctfDetail.numComments || 0} comments
                </Typography>
              </Box>
              <Box mb={1}>
                <Typography variant="p">{ctfDetail.description}</Typography>
              </Box>
            </Box>
          </Paper>
          <Paper
            style={{
              padding: "0.5rem 1rem",
              margin: "0.5rem 0.5rem",
              backgroundColor: "#fffcf9"
            }}
          >
            <Box display="flex" flexDirection="column" m={2}>
              <Box mb={1}>
                <Typography fontSize="25px" fontWeight="500" >Details</Typography>
              </Box>
              <Box mb={2}>
                {ctfDetail.details.split("\n").map(e => (
                  <Box mb={1}>
                    <Typography variant="p">{e}</Typography>
                  </Box>
                ))}
              </Box>
            </Box>
          </Paper>
          <Paper
            style={{
              padding: "0.5rem 1rem",
              margin: "0.5rem 0.5rem",
              backgroundColor: "#fffcf9"
            }}
          >
            <Box display="flex" flexDirection="column" m={2}>
              <Box mb={1} display="flex" flexDirection="row" justifyContent="space-between">
                <Typography fontSize="25px" fontWeight="500" >Comments</Typography>
                <Button
                  variant="outlined" color="primary"
                  onClick={() => { }}>
                  Post Comment
                </Button>
              </Box>
              <Box mb={1}>
                <TextField
                  id="new_comment"
                  // label="Comment"
                  value={""}
                  onChange={() => { }}
                  fullWidth />
              </Box>
              <Box mb={2}>
                <CommentList commentList={ctfDetail.comments} />
              </Box>
            </Box>
          </Paper >
        </Box >

      )
      }
    </>

  );
}

export default function CTFDetailsPage() {

  return (
    <PageLayoutWrapper name="CTF Details">
      <CTFDetails />
    </PageLayoutWrapper>
  );
}