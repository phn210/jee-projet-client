import { Box, Button, Paper, Typography } from "@mui/material";
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import { NavLink } from "react-router-dom";

export default function CTFItem({ ctfData, ...props }) {
  return (
    <>
      {
        props.isOrganizer ? (<Paper
          style={{
            padding: "0.5rem 1rem",
            margin: "0.5rem 0.5rem",
            backgroundColor: "#fffcf9"
          }}
        >
          <Box display="flex" flexDirection="column" m={2}>
            <Box mb={1}>
              <Box display="flex" flexDirection="row"
                justifyContent="space-between"
              >
                <Typography fontSize="25px" fontWeight="500" >{ctfData.name}</Typography>
                <Box display="flex" flexDirection="row-reverse">
                  <Button onClick={(e) => props.onDelete(e, ctfData.ctfId)} >
                    <DeleteOutlineOutlinedIcon />
                  </Button>
                  <Typography fontSize="16px" fontWeight="300" mr={2}>
                    {ctfData.numViews || 0} views, {ctfData.numComments || 0} comments
                  </Typography>
                </Box>
              </Box>
            </Box>
            <Box mb={2}>
              <Typography fontStyle="italic">
                {ctfData.organizer ? ctfData.organizer.name : "Untitled"}
                &nbsp;-&nbsp;
                {ctfData.organizer ? ctfData.organizer.contact : "No contact"}
              </Typography>
            </Box>
            <Box mb={1}>
              <Typography variant="p">{
                (ctfData.description && ctfData.description.length > 250)
                  ? ctfData.description.slice(0, 250).trim() + "..."
                  : ctfData.description
              }</Typography>
            </Box>
          </Box>
        </Paper >) : (<NavLink
          to={`/ctfs/${ctfData.ctfId}`}
          style={{
            width: "100%",
            textDecoration: "none",
            "&:hover": {
              textDecoration: "none",
            },
          }}
        >
          <Paper
            style={{
              padding: "0.5rem 1rem",
              margin: "0.5rem 0.5rem",
              backgroundColor: "#fffcf9"
            }}
          >
            <Box display="flex" flexDirection="column" m={2}>
              <Box mb={1}>
                <Box display="flex" flexDirection="row"
                  justifyContent="space-between"
                >
                  <Typography fontSize="25px" fontWeight="500" >{ctfData.name}</Typography>
                  <Typography fontSize="16px" fontWeight="300" >
                    {ctfData.numViews || 0} views, {ctfData.numComments || 0} comments
                  </Typography>
                </Box>
              </Box>
              <Box mb={2}>
                <Typography fontStyle="italic">
                  {ctfData.organizer ? ctfData.organizer.name : "Untitled"}
                  &nbsp;-&nbsp;
                  {ctfData.organizer ? ctfData.organizer.contact : "No contact"}
                </Typography>
              </Box>
              <Box mb={1}>
                <Typography variant="p">{
                  (ctfData.description && ctfData.description.length > 250)
                    ? ctfData.description.slice(0, 250).trim() + "..."
                    : ctfData.description
                }</Typography>
              </Box>
            </Box>
          </Paper >
        </NavLink>)
      }
    </>
  );
}