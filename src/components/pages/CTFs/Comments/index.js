import {
  Avatar, Box, Divider, List, ListItem,
  ListItemAvatar, Typography
} from "@mui/material";

export function CommentList({ commentList }) {
  return (
    <Box>
      <List sx={{ width: '100%' }}>
        <Divider mb={1} />
        {commentList && commentList.length > 0 && (
          commentList.map(comment => (
            <>
              <ListItem alignItems="center">
                <ListItemAvatar>
                  <Avatar alt={comment.user} src="./" />
                </ListItemAvatar>
                <Box display="flex" flexDirection="column">
                  <Typography>{comment.user}</Typography>
                  <Typography>{comment.comment}</Typography>
                </Box>
              </ListItem>
              <Divider mb={1} />
            </>
          ))
        )}
      </List>
    </Box>
  );
}