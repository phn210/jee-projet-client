import { Box, Paper, Typography } from "@mui/material";

export default function NewDetails({ newData }) {
  return (
    <Paper
      style={{
        padding: "0 1rem",
        margin: "0.5rem 0.5rem",
        backgroundColor: "#fffcf9"
      }}
    >
      <Box display="flex" flexDirection="column" m={2}>
        <Box mb={1}>
          <Typography fontSize="25px" fontWeight="500" >{newData.title}</Typography>
        </Box>
        <Box mb={2}>
          <Typography fontStyle="italic">
            {newData.organizer.name}
            &nbsp;-&nbsp;
            {newData.organizer.contact}
          </Typography>
        </Box>
        <Box mb={1}>
          <Typography variant="p">{newData.content}</Typography>
        </Box>
      </Box>
    </Paper>
  );
}