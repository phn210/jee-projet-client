import { Box } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";
import { fetchAllNewsData } from "../../../redux/publicDataSlice";
import { QUERY_STATE } from "../../../configs/constance";
import Empty from "../../Icon/Empty";
import NewDetails from "./NewDetails";

export function NewList() {
  const dispatch = useDispatch();
  const { fetchingAllNewsStatus, news } = useSelector((state) => state.publicDataSlice);

  useEffect(() => {
    if (fetchingAllNewsStatus === QUERY_STATE.INITIAL) dispatch(fetchAllNewsData());
  }, [fetchingAllNewsStatus, news]);

  return (
    <Box
      display="flex" justifyContent="start"
      flexDirection="column"
      padding={2}
    >
      {(news && news.length) ?
        // <>
        news.map(data => <NewDetails newData={data} />)
        // </>
        : <Empty title="There are no news" iconProps={{ style: { fontSize: "60px" } }} />
      }
    </Box >
  );
}

export default function NewsPage() {
  return (
    <PageLayoutWrapper name="News">
      <NewList />
    </PageLayoutWrapper>
  );
}