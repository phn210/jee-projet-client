import { Box, Button, Paper, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";

export function DiscussionDetails() {
  return (<>Discussion</>);
}

export function DiscussionList() {
  return (<>List</>);
}

export function Discussion() {
  return (<>ASD</>);
}

export default function DiscussionPage() {
  return (
    <PageLayoutWrapper name="Discussion">
      <Discussion />
    </PageLayoutWrapper>
  );
}