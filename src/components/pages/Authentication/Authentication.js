import {
  Box, Button, MenuItem, Paper, Select,
  TextField, Typography, useMediaQuery
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { ROLE } from "../../../configs/constance";
import { signUp, signIn } from "../../../redux/userDataSlice";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";

export function Authentication() {
  const theme = useTheme();
  const onlyXs = useMediaQuery(theme.breakpoints.only("xs"));
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [signInData, setSignInData] = useState({});
  const [signUpData, setSignUpData] = useState({});

  const handleSignInUsername = (e) => {
    e.preventDefault();
    let update = signInData;
    update.username = e.target.value;
    setSignInData(update);
  }

  const handleSignInPassword = (e) => {
    e.preventDefault();
    let update = signInData;
    update.password = e.target.value;
    setSignInData(update);
  }

  const handleSignUpUsername = (e) => {
    e.preventDefault();
    let update = signUpData;
    update.username = e.target.value;
    setSignUpData(update);
  }

  const handleSignUpEmail = (e) => {
    e.preventDefault();
    let update = signUpData;
    update.email = e.target.value;
    setSignUpData(update);
  }

  const handleSignUpRole = (e) => {
    e.preventDefault();
    let update = signInData;
    update.role = e.target.value;
    setSignUpData(update);
  }

  const handleSignUpPassword = (e) => {
    e.preventDefault();
    let update = signUpData;
    update.password = e.target.value;
    setSignUpData(update);
  }

  const onSubmitSignIn = (e) => {
    console.log(signInData);
    dispatch(signIn(signInData));
    navigate("/");
  }

  const onSubmitSignUp = (e) => {
    console.log(signUpData);
    dispatch(signUp(signUpData));
    navigate("/");
  }

  return (
    <Box
      display="flex" justifyContent="space-around"
      flexDirection={onlyXs ? "column" : "row"}
      padding={2}
    >
      <Paper style={{ backgroundColor: theme.palette.grey[100] }}>
        <Box
          display="flex"
          flexDirection="column"
          alignItems="center"
          marginLeft={4}
          marginRight={4}
          padding={2}
        >
          <Box mb={2}>
            <Typography component="h4" variant="h4">
              Sign In
            </Typography>
          </Box>
          <Box mb={2}>
            <TextField
              id="signin_username"
              label="Username"
              value={signInData.username}
              onChange={e => handleSignInUsername(e)}
              fullWidth />
          </Box>
          <Box mb={2}>
            <TextField
              id="signin_password"
              label="Password"
              type="password"
              value={signInData.password}
              onChange={e => handleSignInPassword(e)}
            />
          </Box>
          <Box mb={2}>
            <Button
              variant="contained" color="primary"
              onClick={() => onSubmitSignIn()}>
              Sign in
            </Button>
          </Box>
          {/* <Box mb={2}>
            <Typography style={{ color: 'red' }}>
              {"Error message"}
            </Typography>
          </Box> */}

        </Box>
      </Paper>

      <Paper style={{ backgroundColor: theme.palette.grey[100] }}>
        <Box
          display="flex"
          flexDirection="column"
          alignItems="center"
          marginLeft={4}
          marginRight={4}
          padding={2}
        >
          <Box mb={2}>
            <Typography component="h4" variant="h4">
              Sign Up
            </Typography>
          </Box>
          <Box mb={2} width="100%">
            <Select
              id="signup_role"
              value={signUpData.role}
              onChange={e => handleSignUpRole(e)}
              fullWidth
            >
              <MenuItem value={ROLE.PARTICIPANT}>{ROLE.PARTICIPANT}</MenuItem>
              <MenuItem value={ROLE.ORGANIZER}>{ROLE.ORGANIZER}</MenuItem>
              <MenuItem value={ROLE.ADMIN}>{ROLE.ADMIN}</MenuItem>
            </Select>
          </Box>
          <Box mb={2}>
            <TextField
              id="signup_username"
              label="Username"
              value={signUpData.username}
              onChange={e => handleSignUpUsername(e)} />
          </Box>
          <Box mb={2}>
            <TextField
              id="signup_email"
              label="Email"
              type="email"
              value={signUpData.email}
              onChange={e => handleSignUpEmail(e)} />
          </Box>
          <Box mb={2}>
            <TextField
              id="signup_password"
              label="Password"
              type="password"
              value={signUpData.password}
              onChange={e => handleSignUpPassword(e)}
            />
          </Box>
          <Box mb={2}>
            <Button
              variant="contained" color="primary"
              onClick={() => onSubmitSignUp()}>
              Sign up
            </Button>
          </Box>
          {/* <Box mb={2}>
            <Typography style={{ color: 'red' }}>
              {"Error message"}
            </Typography>
          </Box> */}

        </Box>
      </Paper>
    </Box >
  );
}

export default function AuthenticationPage() {
  return (
    <PageLayoutWrapper name="Account">
      <Authentication />
    </PageLayoutWrapper>
  );
}