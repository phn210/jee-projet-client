import { Box, Button, Paper, TextField, Typography } from "@mui/material";
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import { useState } from "react";
import { useDispatch } from "react-redux";
import { createCTF } from "../../../redux/userDataSlice";

export function ChallengeForm({ challengeId, onChangeData, onRemove }) {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [details, setDetails] = useState("");
  const [result, setResult] = useState("");

  const handleDataChange = (e, key) => {
    // e.preventDefault();
    switch (key) {
      case "name":
        setName(e.nativeEvent.target.value);
        break;
      case "description":
        setDescription(e.nativeEvent.target.value);
        break;
      case "details":
        setDetails(e.nativeEvent.target.value);
        break;
      case "result":
        setResult(e.nativeEvent.target.value);
        break;
      default:
        break;
    }
    onChangeData(challengeId, key, e.nativeEvent.target.value);
  }

  return (
    <Paper style={{ backgroundColor: "default", width: "100%" }}>
      <Box display="flex" flexDirection="column" alignItems="start" m={2}>
        <Box display="flex" flexDirection="row">
          <Box mb={1}>
            <Typography mb={1} >Name</Typography>
            <TextField
              id="chall_name"
              value={name}
              sx={{ minWidth: "400px" }}
              onChange={(e) => handleDataChange(e, "name")}
            />
          </Box>
          <Button onClick={(e) => onRemove(e, challengeId)} >
            <DeleteOutlineOutlinedIcon />
          </Button>
        </Box>
        <Box mb={1} sx={{ width: "100%" }}>
          <Typography mb={1} >Description</Typography>
          <TextField
            id="chall_desc"
            value={description}
            fullWidth
            onChange={(e) => handleDataChange(e, "description")}
          />
        </Box>
        <Box mb={1} sx={{ width: "100%" }}>
          <Typography mb={1} >Details</Typography>
          <TextField
            id="chall_details"
            value={details}
            fullWidth
            onChange={(e) => handleDataChange(e, "details")}
          />
        </Box>
        <Box mb={1} sx={{ width: "100%" }}>
          <Typography mb={1} >Result</Typography>
          <TextField
            id="chall_res"
            value={result}
            fullWidth
            onChange={(e) => handleDataChange(e, "result")}
          />
        </Box>
      </Box>
    </Paper>
  );
}

export default function CreateCTF({ onUpdateView }) {
  const dispatch = useDispatch();
  const [ctf, setCtf] = useState({});
  const [challenges, setChallenges] = useState([]);

  const onSubmitData = (e, viewIndex) => {
    const newCTF = {
      ...ctf,
      challenges: challenges
    }
    console.log(newCTF);
    dispatch(createCTF(newCTF));
    onUpdateView(e, viewIndex);
  }

  const onUpdateCTFData = (e, key) => {
    // e.preventDefault();
    let newCtf = ctf;
    console.log(e.nativeEvent.target.value);
    newCtf[key] = e.nativeEvent.target.value;
    newCtf.challenges = challenges;
    setCtf(newCtf);
  }

  const onUpdateChallengesData = (challengeId, key, data) => {
    const newChallenges = challenges;
    console.log(challengeId);
    console.log(key);
    console.log(data);
    challenges[challengeId][key] = data;
    // newChallenges[challengeId].name = data.name;
    // newChallenges[challengeId].description = data.description;
    // newChallenges[challengeId].details = data.details;
    // newChallenges[challengeId].result = data.result;
    setChallenges(newChallenges);
    console.log("update challenges");
  }

  const handleAddChallenge = (e) => {
    const newChallenges = [...challenges];
    newChallenges.push({
      challengeId: newChallenges.length,
      name: "",
      description: "",
      details: "",
      result: "",
    });
    setChallenges(newChallenges);
  }

  const handleRemoveChallenge = (e, challengeId) => {
    console.log(challenges.length);
    const newChallenges = challenges;
    newChallenges.splice(challengeId, 1);
    console.log(newChallenges.length)
    setChallenges(newChallenges);
  }

  return <Paper
    style={{
      padding: "1rem 2rem",
      margin: "0.5rem 0.5rem",
      backgroundColor: "#fffcf9"
    }}
  >
    <Box display="flex" flexDirection="column" alignItems="start" m={2}>
      <Typography variant="h5" mb={2} >CTF Information</Typography>
      <Box mb={1}>
        <Typography mb={1} >CTF Name</Typography>
        <TextField
          id="ctf_name"
          value={ctf.name}
          sx={{ minWidth: "400px" }}
          onChange={(e) => onUpdateCTFData(e, "name")}
        />
      </Box>
      <Box mb={1}>
        <Typography mb={1} >CTF Cover Image (URL)</Typography>
        <TextField
          id="ctf_cover"
          value={ctf.coverUri}
          sx={{ minWidth: "400px" }}
          onChange={(e) => onUpdateCTFData(e, "coverUri")}
        />
      </Box>
      <Box mb={1} sx={{ width: "100%" }}>
        <Typography mb={1} >Description</Typography>
        <TextField
          id="ctf_desc"
          value={ctf.description}
          sx={{}}
          onChange={(e) => onUpdateCTFData(e, "description")}
          fullWidth
          multiline
        />
      </Box>
      <Box mb={1} sx={{ width: "100%" }}>
        <Typography mb={1} >Details</Typography>
        <TextField
          id="ctf_details"
          value={ctf.details}
          sx={{}}
          onChange={(e) => onUpdateCTFData(e, "details")}
          fullWidth
          multiline
        />
      </Box>
      <Button onClick={(e) => handleAddChallenge(e)} >
        <AddCircleOutlineOutlinedIcon />
      </Button>
      <Typography variant="h5" my={2} >Challenges</Typography>
      {challenges.length > 0 && challenges.map(
        (challenge, index) => <ChallengeForm
          key={index}
          challengeId={challenge.challengeId}
          onChangeData={onUpdateChallengesData}
          onRemove={handleRemoveChallenge}
        />
      )}
    </Box>
    <Box m={2}>
      <Button
        variant="contained" fullWidth
        color="primary"
        onClick={(e) => onSubmitData(e, 1)}>
        Submit
      </Button>
    </Box>
  </Paper>
}