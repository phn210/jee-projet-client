import { Box, Button, Paper, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import PageLayoutWrapper from "../../Wrappers/PageWrapper";
import OrganizerInformation from "./Information";
import CreateCTF from "./CreateCTF";
import CreateNew from "./CreateNew";
import CTFItem from "../CTFs/CTFItem";
import { fetchOrganizerCTFs } from "../../../redux/userDataSlice";
import { QUERY_STATE } from "../../../configs/constance";
import Empty from "../../Icon/Empty";


export function CTFList({ onUpdateView }) {
  const dispatch = useDispatch();
  const { fetchingOrganizerCTFsStatus, organizer, userId } = useSelector((state) => state.userDataSlice);

  useEffect(() => {
    if (fetchingOrganizerCTFsStatus === QUERY_STATE.INITIAL) dispatch(fetchOrganizerCTFs(userId));
  }, [fetchingOrganizerCTFsStatus, organizer, userId]);

  const onDelete = (e, ctfId) => {
    // console.log("Delete CTF", ctfId);
    // dispatch(fetchOrganizerCTFs(userId));
  }

  return (
    <Box
      display="flex" justifyContent="start"
      flexDirection="column"
    >
      <Box m={1}>
        <Button
          variant="contained" fullWidth
          color="primary"
          // style={{ margin: "0 0.5rem 0 0.5rem" }}
          onClick={(e) => onUpdateView(e, 3)}>
          New CTF
        </Button>
      </Box>
      {(organizer.ctfs && organizer.ctfs.length) ?
        organizer.ctfs.map(
          data => <CTFItem ctfData={data} isOrganizer={true} organizer={organizer} onDelete={onDelete} />
        )
        : <Empty title="There are no CTFs" iconProps={{ style: { fontSize: "60px" } }} />
      }
    </Box >
  );
}

export function NewList({ onUpdateView }) {
  return <>NewList</>
}

export function Organizer({ viewIndex }) {
  const [view, setView] = useState(0);

  useEffect(() => {
    setView(viewIndex || 1);
  }, []);

  const handleUpdateView = (e, viewIndex) => {
    setView(viewIndex);
  }

  return (
    <Box display="flex" justifyContent="start"
      flexDirection="column"
      padding={2}>
      <Box mb={2} display="flex" justifyContent="space-between" flexDirection="row">
        <Button
          variant="outlined" fullWidth
          color="primary" style={{ margin: "0 0.5rem 0 0.5rem" }}
          onClick={(e) => handleUpdateView(e, 0)}>
          Information
        </Button>
        <Button
          variant="outlined" fullWidth
          color="primary" style={{ margin: "0 0.5rem 0 0.5rem" }}
          onClick={(e) => handleUpdateView(e, 1)}>
          CTFs
        </Button>
        <Button
          variant="outlined" fullWidth
          color="primary" style={{ margin: "0 0.5rem 0 0.5rem" }}
          onClick={(e) => handleUpdateView(e, 2)}>
          News
        </Button>
      </Box>
      {view === 0 && <OrganizerInformation />}
      {view === 1 && <CTFList onUpdateView={handleUpdateView} />}
      {view === 2 && <NewList onUpdateView={handleUpdateView} />}
      {view === 3 && <CreateCTF onUpdateView={handleUpdateView} />}
      {view === 4 && <CreateNew onUpdateView={handleUpdateView} />}
    </Box>
  );
}

export default function OrganizerPage() {
  return (
    <PageLayoutWrapper name="Organizer">
      <Organizer />
    </PageLayoutWrapper>
  );
}