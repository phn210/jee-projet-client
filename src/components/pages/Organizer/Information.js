import { Avatar, Box, Button, Paper, Typography } from "@mui/material";
import { useSelector } from "react-redux";

export default function OrganizerInformation() {
  const { organizer } = useSelector((state) => state.userDataSlice);

  return (
    <Paper
      style={{
        padding: "1rem 2rem",
        margin: "0.5rem 0.5rem",
        backgroundColor: "#fffcf9"
      }}
    >
      <Box display="flex" flexDirection="row" m={2}>
        <Box>
          {console.log(organizer)}
          <Avatar
            src={organizer && organizer.avatar}
            sx={{ height: '20vw', width: '20vw' }}
          />
        </Box>
        <Box display="flex" flexDirection="column" alignItems="center" ml={4}>
          <Box>
            <Typography variant="h5" mb={1}>{organizer.name || "Untitled"}</Typography>
            <Typography fontStyle="italic" mb={2}>Contact: {organizer.contact || "No contact"}</Typography>
            <Typography>{organizer.ctfs ? organizer.ctfs.length : 0} CTFs</Typography>
            <Typography>{organizer.news ? organizer.news.length : 0} News</Typography>
            {organizer.numViews && <Typography>{organizer.numViews} Views</Typography>}
          </Box>
        </Box>
      </Box>
    </Paper>
  )
}