import { Box, Link, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { NavLink, useLocation } from "react-router-dom";

function BaseLink({ type, to, children, ...props }) {
  return (
    <Link
      {...props}
      href={to}
      style={{
        width: "100%",
        textDecoration: "none",
        "&:hover": {
          textDecoration: "none",
        },
      }}
      target="_self"
    >
      {children}
    </Link>
  );
}

export function FlexibleLinkItem({ type, to, label, Component, ...props }) {
  return (
    <BaseLink type={type} to={to} style={{ width: "100%" }} {...props}>
      <Box display="flex" justifyContent="space-between">
        <Box display="flex">
          {Component?.start}
          <Typography>{label}</Typography>
        </Box>
      </Box>
    </BaseLink>
  );
}

export function DesktopList({ element }) {
  const theme = useTheme();
  const IconTag = element.icon;
  const location = useLocation();

  return (
    <NavLink
      to={element.to}
      style={{
        width: "100%",
        textDecoration: "none",
        "&:hover": {
          textDecoration: "none",
        },
        padding: "0.5rem 0.5rem 0rem 0.5rem",
        borderRadius: "3px"
      }}
    >
      <Box display="flex" justifyContent="center" alignItems="center" flexDirection="column" style={{
        padding: "0.25rem",
        borderRadius: "5px",
        "&:hover": {
          backgroundColor: "#F2F2F2",
        },
      }}>
        <Box
          style={element.isActive(null, location) ? {
            margin: "0.25rem",
            width: "55px",
            height: "30px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: "3px",
            backgroundColor: "rgba(226, 160, 37, 0.15)"
          } : {
            margin: "0.25rem",
            width: "55px",
            height: "30px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: "3px",
          }}
        >
          <IconTag style={{
            width: 25,
            fontSize: "18px",
            color: theme.palette.text.secondary,
          }} />
        </Box>
        <Typography color="textSecondary" style={{ fontSize: "14px", fontWeight: 400 }}>
          {element.label}
        </Typography>
      </Box>
    </NavLink>
  );
}
