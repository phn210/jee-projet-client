import { Box, Drawer, List, ListItem } from "@mui/material";
import { useSelector } from "react-redux";
import { LayoutConfig } from "../../../../configs/constance";
import { DesktopList } from "../FlexibleLink";

export default function Sidebar() {
  const { role } = useSelector((state) => state.userDataSlice);

  function onMouseEnter(index) {
    const subTab = document.getElementById(`subTab_${index}`);
    if (subTab) subTab.style.transform = "translateX(38%)";
  }

  function onMouseLeave(index) {
    const subTab = document.getElementById(`subTab_${index}`);
    if (subTab) subTab.style.transform = "translateX(-100%)";
  }

  function onClick(index) {
    const subTab = document.getElementById(`subTab_${index}`);
    if (subTab) subTab.style.transform = "translateX(38%)";
  }

  function Layout() {
    return (
      <Box
        height="100%"
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="space-between"
        style={{ width: "100px", paddingTop: "2rem", paddingBottom: "2rem" }}
      >
        <Box display="flex" flexDirection="column" alignItems="center" width="100%">
          {/* <Logo /> */}
          <List style={{ padding: 0, width: "100%" }}>
            {LayoutConfig.filter(layout => layout.roles.includes(role))
              .map((element, index) => {
                return (
                  <ListItem
                    key={index}
                    style={{
                      cursor: "pointer",
                      textDecoration: "none",
                      padding: 0,
                    }}
                    onMouseEnter={() => onMouseEnter(index)}
                    onMouseLeave={() => onMouseLeave(index)}
                    onClick={() => onClick(index)}
                  >
                    <DesktopList element={element} />
                  </ListItem>
                );
              })}
          </List>
        </Box>
      </Box>
    );
  }

  return (
    <>
      <Drawer anchor="left" PaperProps={{
        backgroundColor: "#FFFFFF",
        padding: 0,
        zIndex: 10001,
      }} variant="permanent">
        <Layout />
      </Drawer>
    </>
  );
}
