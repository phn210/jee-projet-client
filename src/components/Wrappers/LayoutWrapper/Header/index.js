import { Box } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from 'react-router-dom';
import { SigninButton, SignoutButton } from "../../../Buttons";
import { useEffect } from "react";
import { QUERY_STATE } from "../../../../configs/constance";
import { fetchUserData } from "../../../../redux/userDataSlice";

export default function Header() {
  const dispatch = useDispatch();
  const { fetchingUserDataStatus, isLogin } = useSelector((state) => state.userDataSlice);
  const location = useLocation();

  useEffect(() => {
    if (fetchingUserDataStatus === QUERY_STATE.INITIAL) dispatch(fetchUserData());
  }, [fetchingUserDataStatus, isLogin])

  return (
    <Box style={{
      height: 65,
      display: "flex",
      alignItems: "center",
      position: "sticky",
      zIndex: 1000,
      top: 0,
      flexDirection: 'row-reverse',
      padding: "0rem 3rem",
      justifyContent: "space-between",
      backgroundColor: "#fbf0e1",
    }}>
      <Box display="flex">
        {isLogin ? <SignoutButton /> : (
          !location.pathname.includes('auth') && <SigninButton />
        )}
      </Box>
    </Box>
  );
}
