import { Box, styled, useMediaQuery } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import Footer from "./Footer";
import Header from "./Header";
import Sidebar from "./Sidebar";

export default function LayoutWrapper({ children }) {

  return (
    <Box position="relative">
      <Sidebar />
      <Box style={{ marginLeft: 100 }}>
        <Header />
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="space-between"
          style={{ minHeight: "calc(100vh - 80px)" }}
        >
          <Box>{children}</Box>
          <Footer />
        </Box>
      </Box>
    </Box>
  );
}
