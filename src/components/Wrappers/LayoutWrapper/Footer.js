import { Box, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";

export default function Footer() {
  const theme = useTheme();

  return (
    <Box style={{
      padding: "0.5rem 3rem",
      display: "flex",
      justifyContent: "center",
      alignItems: "flex-start",
      [theme.breakpoints.down("md")]: {
        padding: "1rem 2rem",
      },
      [theme.breakpoints.only("xs")]: {
        alignItems: "center",
        flexDirection: "column-reverse",
        padding: "1rem",
      },
      // opacity: 0.1
    }}>
      <Typography color="textSecondary" style={{ fontSize: "14px" }}>
        COPYRIGHT © {new Date().getFullYear()}&nbsp;
        Cryptis M1 - Ho Nguyen PHAM & Romain LOPEZ -
        &nbsp;All right reserved
      </Typography>
    </Box >
  );
}
