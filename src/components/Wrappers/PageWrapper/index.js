import { Box, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { QUERY_STATE } from "../../../configs/constance";
import { fetchUserData } from "../../../redux/userDataSlice";

export default function PageLayoutWrapper({ name, children }) {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { fetchingUserDataStatus, isLogin } = useSelector((state) => state.userDataSlice);

  useEffect(() => {
    if (fetchingUserDataStatus === QUERY_STATE.INITIAL) dispatch(fetchUserData());
  }, [fetchingUserDataStatus, isLogin]);

  return (
    <Box>
      <Box
        style={{
          padding: "1rem 3rem",
          position: "relative",
          backgroundColor: "#fbf0e1",
          [theme.breakpoints.down("md")]: {
            padding: "1rem 2rem",
          },
          [theme.breakpoints.only("xs")]: {
            padding: "1rem",
          },
        }}
      >
        <Typography
          style={{
            color: "#111111",
            fontSize: "40px",
            fontWeight: 700,
          }}
        >
          {name}
        </Typography>
      </Box>
      {children}
    </Box>

  )
}