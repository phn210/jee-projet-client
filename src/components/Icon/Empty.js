import { Box, Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { EmptyBoxIcon } from ".";

export default function Empty({ title, iconProps, titleProps, ...props }) {
  const theme = useTheme();

  return (
    <Box display="flex" flexDirection="column" alignItems="center" py={2} {...props}>
      <EmptyBoxIcon style={{ color: theme.palette.text.secondary }} {...iconProps} />
      {title && (
        <Typography color="textSecondary" display="inline" {...titleProps}>
          {title}
        </Typography>
      )}
    </Box>
  );
}
