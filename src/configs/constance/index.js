import { Icon1, Icon2, Icon3, Icon4 } from "../../components/Icon";

export const BACKEND_ROOT_URL = "http://192.168.59.155:8080"
// export const BACKEND_ROOT_URL = "http://localhost:8080"

export const ROLE = {
  GUEST: "GUEST",
  PARTICIPANT: "PARTICIPANT",
  ORGANIZER: "ORGANIZER",
  ADMIN: "ADMIN",
};

export const QUERY_STATE = {
  INITIAL: "INITIAL",
  FETCHING: "FETCHING",
  SUCCESS: "SUCCESS",
  FAIL: "FAIL",
};

export const LayoutConfig = [
  {
    label: "News",
    icon: Icon2,
    to: "/news",
    isActive: (_, location) => location.pathname.includes("news"),
    subTitle: "News",
    subTab: [],
    roles: [ROLE.ADMIN, ROLE.ORGANIZER, ROLE.PARTICIPANT, ROLE.GUEST]
  },
  {
    label: "Admin",
    icon: Icon1,
    to: "/admin",
    isActive: (_, location) => location.pathname.includes("admin"),
    subTitle: "Admin",
    subTab: [],
    roles: [ROLE.ADMIN]
  },
  {
    label: "Organizer",
    icon: Icon1,
    to: "/organizer",
    isActive: (_, location) => location.pathname.includes("organizer"),
    subTitle: "Organizer",
    subTab: [],
    roles: [ROLE.ORGANIZER]
  },
  {
    label: "CTFs",
    icon: Icon3,
    to: "/ctfs",
    isActive: (_, location) => location.pathname.includes("ctfs"),
    subTitle: "DAOs",
    subTab: [],
    roles: [ROLE.ADMIN, ROLE.ORGANIZER, ROLE.PARTICIPANT, ROLE.GUEST]
  },
  {
    label: "Challenges",
    icon: Icon3,
    to: "/challenges",
    isActive: (_, location) => location.pathname.includes("challenges"),
    subTitle: "Challenges",
    subTab: [],
    roles: [ROLE.ADMIN, ROLE.ORGANIZER, ROLE.PARTICIPANT, ROLE.GUEST]
  },
  {
    label: "Team",
    icon: Icon4,
    to: "/team",
    isActive: (_, location) => location.pathname.includes("team"),
    subTitle: "Team",
    subTab: [],
    roles: [ROLE.ADMIN, ROLE.ORGANIZER, ROLE.PARTICIPANT, ROLE.GUEST]
  },
  {
    label: "Discussion",
    icon: Icon4,
    to: "/discussion",
    isActive: (_, location) => location.pathname.includes("discussion"),
    subTitle: "Discussion",
    subTab: [],
    roles: [ROLE.ORGANIZER, ROLE.PARTICIPANT]
  },
];