import { Route, Routes } from "react-router-dom";
import { useSelector } from "react-redux";
import AdminPage from "../components/pages/Admin";
import AuthenticationPage from "../components/pages/Authentication/Authentication";
import ChallengeDetailsPage from "../components/pages/Challenges/ChallengeDetails";
import ChallengesPage from "../components/pages/Challenges";
import CTFDetailsPage from "../components/pages/CTFs/CTFDetails";
import CTFsPage from "../components/pages/CTFs";
import DiscussionPage from "../components/pages/Discussion";
import NewsPage from "../components/pages/News";
import OrganizerPage from "../components/pages/Organizer";
import TeamPage from "../components/pages/Team";
import { ROLE } from "../configs/constance";

export default function RouteApp() {
  const { role } = useSelector((state) => state.userDataSlice);

  return (
    <Routes>
      <Route path="/" element={<NewsPage />} />
      <Route path="/auth" element={<AuthenticationPage />} />
      <Route path="/news" element={<NewsPage />} />
      {
        role === ROLE.ADMIN && <Route path="/admin" element={<AdminPage />} />
      }
      {role === ROLE.ORGANIZER && <Route path="/organizer" element={<OrganizerPage />} />}
      <Route path="/ctfs" element={<CTFsPage />} />
      <Route path="/ctfs/:ctfId" element={<CTFDetailsPage />} />
      <Route path="/challenges" element={<ChallengesPage />} />
      <Route path="/challenges/:challengeId" element={<ChallengeDetailsPage />} />
      <Route path="/team" element={<TeamPage />} />
      {
        role !== ROLE.GUEST && <Route path="/discussion" element={<DiscussionPage />} />
      }
      <Route path="/*" element={<NewsPage />} />
    </Routes>
  );
}
