import { IconButton } from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import { SnackbarProvider } from "notistack";
import { createRef } from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import LayoutWrapper from "../components/Wrappers/LayoutWrapper";
import store from "../redux/store";
import RouteApp from "./RouteApp";

export default function ProviderApp() {
  const notistackRef = createRef();
  const onClickDismiss = (key) => () => {
    notistackRef.current.closeSnackbar(key);
  };

  return (
    <Provider store={store}>
      <Router>
        <SnackbarProvider
          maxSnack={3}
          ref={notistackRef}
          action={(key) => (
            <IconButton size="small" onClick={onClickDismiss(key)}>
              <CloseIcon style={{ color: "#FFFFFF" }} />
            </IconButton>
          )}
        >
          <LayoutWrapper>
            <RouteApp />
          </LayoutWrapper>
        </SnackbarProvider>
      </Router>
    </Provider>
  );
}
