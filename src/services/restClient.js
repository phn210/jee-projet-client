import axios from "axios";
import { BACKEND_ROOT_URL } from "../configs/constance";

const responseBody = (res) => res.data;

export const createClient = (apiRoot, apiConfig = {}) => {
  return {
    get: (url, config) => axios.get(`${apiRoot}${url}`, { ...config, ...apiConfig }).then(responseBody),
    post: (url, body, config) => axios.post(`${apiRoot}${url}`, body, { ...config, ...apiConfig }).then(responseBody),
    del: (url, config) => axios.delete(`${apiRoot}${url}`, { ...config, ...apiConfig }).then(responseBody),
    put: (url, body, config) => axios.put(`${apiRoot}${url}`, body, { ...config, ...apiConfig }).then(responseBody),
  };
};

export const defaultClient = createClient(BACKEND_ROOT_URL);
