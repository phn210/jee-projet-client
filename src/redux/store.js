import { configureStore } from "@reduxjs/toolkit";
import publicDataReducer from "./publicDataSlice";
import userDataReducer from "./userDataSlice";

export default configureStore({
  reducer: {
    publicDataSlice: publicDataReducer,
    userDataSlice: userDataReducer,
  },
});
