import { createSlice } from "@reduxjs/toolkit";
import { QUERY_STATE } from "../configs/constance";
import { defaultClient } from "../services/restClient";

const MOCK_NEWS = [
  {
    title: "New CTF in October",
    content: "Veniam est in adipisicing laborum enim excepteur nostrud Lorem minim. Esse aliqua eiusmod duis dolor aliquip. Adipisicing in eu velit velit labore mollit sint veniam est cupidatat. Proident consequat voluptate velit reprehenderit eu esse sit deserunt esse.",
    organizer: {
      name: "Unilim",
      contact: "unilim.fr"
    }
  },
  {
    title: "Crypto CTF 2024",
    content: "Dolor nostrud irure non excepteur deserunt minim ex. Ad velit dolor dolore est. Et cillum nostrud magna consectetur non laborum deserunt qui aliqua. Do amet pariatur aute ex esse id quis sint mollit nulla quis laboris sint qui. Excepteur aliqua id minim ut ut nostrud ut deserunt laboris esse magna sit in. Ea pariatur quis nostrud consequat velit exercitation velit est qui dolor.",
    organizer: {
      name: "XLIM",
      contact: "xlim@unilim.fr"
    }
  },
  {
    title: "First major CTF of the year!",
    content: "Esse cillum cillum ea culpa id quis quis anim eu. Proident reprehenderit mollit ut anim cillum qui voluptate deserunt duis quis ex exercitation consequat nisi. Minim nostrud consequat proident do eiusmod. Ullamco irure laborum sint elit occaecat aliquip cupidatat irure. Elit excepteur consectetur eiusmod excepteur exercitation do commodo aliqua elit consectetur dolore. Dolore cupidatat magna qui consequat fugiat nostrud minim elit elit cillum proident consequat. Consectetur minim cillum aliquip eiusmod nostrud eiusmod ipsum tempor dolor dolor anim.",
    organizer: {
      name: "France CTF",
      contact: "09 87 65 43 21"
    }
  }
];

const MOCK_CTFS = [
  {
    ctfId: 0,
    name: "October CTF 2023",
    description: "Nostrud exercitation deserunt eu culpa magna cillum anim ullamco consequat. Commodo deserunt elit incididunt nisi. Voluptate cillum ullamco elit reprehenderit velit fugiat est. Reprehenderit occaecat aliqua mollit cupidatat id eiusmod consectetur reprehenderit est in enim anim. Aliquip enim officia elit consequat tempor deserunt velit occaecat voluptate eu aute cupidatat.",
    details: "Culpa eu duis labore pariatur in exercitation eu id consectetur anim irure cupidatat. Ea aliqua reprehenderit ex commodo esse mollit deserunt irure. Ut commodo laborum eiusmod proident dolor mollit nulla cupidatat in ea et labore magna. Pariatur sint proident minim do adipisicing voluptate mollit magna.\nPariatur do aute officia Lorem anim aliqua dolor voluptate ex veniam ad pariatur consectetur nisi.Qui nostrud mollit nulla non ex fugiat ipsum.Eu id voluptate non mollit cupidatat.Ullamco laborum velit incididunt occaecat.\nLaboris tempor consectetur tempor irure id ipsum exercitation tempor.Ea est culpa sit fugiat duis esse magna consequat enim ipsum culpa proident.Excepteur consectetur duis cillum incididunt officia minim aliqua ipsum exercitation.Proident ipsum et in qui pariatur minim tempor nisi.\nPariatur nostrud ut adipisicing aute velit.Deserunt ex aliqua id laboris dolor minim cupidatat Lorem officia consectetur veniam deserunt amet laboris.Incididunt enim tempor deserunt irure exercitation veniam est dolore sint consectetur ullamco.Esse ipsum nulla mollit mollit aliqua aute incididunt.",
    result: "Laboris esse commodo mollit cillum culpa consectetur irure. Lorem ullamco sunt sint ad dolore minim. Veniam ad anim eu deserunt nostrud reprehenderit eu sint velit ullamco. Laboris dolore exercitation excepteur esse commodo reprehenderit fugiat dolore consectetur voluptate. Aute qui dolor nostrud magna in fugiat dolore.\nConsequat qui Lorem duis eu Lorem eiusmod cupidatat mollit officia laboris cupidatat qui ipsum cupidatat. Irure et consectetur veniam elit nisi. Enim anim sint officia consequat ut enim quis pariatur commodo incididunt id. Qui ea ad labore dolor occaecat occaecat duis consequat ea velit. Dolor ex nostrud adipisicing non labore duis dolore non in sit fugiat irure anim. Id tempor nostrud veniam sunt.",
    coverUri: "https://media.istockphoto.com/id/1250701216/photo/capture-the-flag.jpg?s=612x612&w=0&k=20&c=tFCMSI_n45m3l28bQsYNAYAo9E0ym8Gne9ZNaViIdC0=",
    organizer: {
      name: "Unilim",
      contact: "unilim.fr"
    },
    challenges: [
      {
        challengeId: 0,
        name: "Hacking Challenge 1",
        description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
        details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
        result: "{CTF:HACK1}",
      },
      {
        challengeId: 1,
        name: "Hacking Challenge 2",
        description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
        details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
        result: "{CTF:HACK2}",
      },
      {
        challengeId: 2,
        name: "RE Challenge 1",
        description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
        details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
        result: "{CTF:RE1}",
      },
      {
        challengeId: 3,
        name: "Crypto Challenge 1",
        description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
        details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
        result: "{CTF:CRYPTO1}",
      }
    ],
    participants: ["phn2211", "romain981"],
    result: "",
    numViews: 1203,
    numComments: 15,
    comments: [
      {
        user: "phn2211",
        comment: "This is interesting"
      },
      {
        user: "romain981",
        comment: "How can I participate?"
      },
      {
        user: "romain981",
        comment: "Wow, looks cool!"
      }
    ]
  },
  {
    ctfId: 0,
    name: "Crypto CTF 2024",
    description: "Nostrud exercitation deserunt eu culpa magna cillum anim ullamco consequat. Commodo deserunt elit incididunt nisi. Voluptate cillum ullamco elit reprehenderit velit fugiat est. Reprehenderit occaecat aliqua mollit cupidatat id eiusmod consectetur reprehenderit est in enim anim. Aliquip enim officia elit consequat tempor deserunt velit occaecat voluptate eu aute cupidatat.",
    details: "Culpa eu duis labore pariatur in exercitation eu id consectetur anim irure cupidatat. Ea aliqua reprehenderit ex commodo esse mollit deserunt irure. Ut commodo laborum eiusmod proident dolor mollit nulla cupidatat in ea et labore magna. Pariatur sint proident minim do adipisicing voluptate mollit magna.\nPariatur do aute officia Lorem anim aliqua dolor voluptate ex veniam ad pariatur consectetur nisi.Qui nostrud mollit nulla non ex fugiat ipsum.Eu id voluptate non mollit cupidatat.Ullamco laborum velit incididunt occaecat.\nLaboris tempor consectetur tempor irure id ipsum exercitation tempor.Ea est culpa sit fugiat duis esse magna consequat enim ipsum culpa proident.Excepteur consectetur duis cillum incididunt officia minim aliqua ipsum exercitation.Proident ipsum et in qui pariatur minim tempor nisi.\nPariatur nostrud ut adipisicing aute velit.Deserunt ex aliqua id laboris dolor minim cupidatat Lorem officia consectetur veniam deserunt amet laboris.Incididunt enim tempor deserunt irure exercitation veniam est dolore sint consectetur ullamco.Esse ipsum nulla mollit mollit aliqua aute incididunt.",
    result: "Laboris esse commodo mollit cillum culpa consectetur irure. Lorem ullamco sunt sint ad dolore minim. Veniam ad anim eu deserunt nostrud reprehenderit eu sint velit ullamco. Laboris dolore exercitation excepteur esse commodo reprehenderit fugiat dolore consectetur voluptate. Aute qui dolor nostrud magna in fugiat dolore.\nConsequat qui Lorem duis eu Lorem eiusmod cupidatat mollit officia laboris cupidatat qui ipsum cupidatat. Irure et consectetur veniam elit nisi. Enim anim sint officia consequat ut enim quis pariatur commodo incididunt id. Qui ea ad labore dolor occaecat occaecat duis consequat ea velit. Dolor ex nostrud adipisicing non labore duis dolore non in sit fugiat irure anim. Id tempor nostrud veniam sunt.",
    coverUri: "https://miro.medium.com/v2/resize:fit:1178/1*-oNPWS2F4TMge19BuO8bPg.png",
    organizer: {
      name: "XLIM",
      contact: "xlim@unilim.fr"
    },
    challenges: [
      {
        challengeId: 0,
        name: "Hacking Challenge 1",
        description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
        details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
        result: "{CTF:HACK1}",
      },
      {
        challengeId: 1,
        name: "Hacking Challenge 2",
        description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
        details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
        result: "{CTF:HACK2}",
      },
      {
        challengeId: 2,
        name: "RE Challenge 1",
        description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
        details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
        result: "{CTF:RE1}",
      },
      {
        challengeId: 3,
        name: "Crypto Challenge 1",
        description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
        details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
        result: "{CTF:CRYPTO1}",
      }
    ],
    participants: [],
    result: "",
    numViews: 2401,
    numComments: 45,
    comments: [],
  }
]

const MOCK_CHALLENGES = [
  {
    challengeId: 0,
    name: "Hacking Challenge 1",
    description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
    details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
    result: "{CTF:CRYPTO1}",
    submisisons: [
      {
        username: "phn2211",
        isCorrect: true,
      },
      {
        username: "par3",
        isCorrect: false,
      },
    ]
  },
  {
    challengeId: 1,
    name: "Hacking Challenge 2",
    description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
    details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
    submisisons: [
      {
        username: "phn2211",
        isCorrect: false,
      },
      {
        username: "phn210",
        isCorrect: false,
      },
    ]
  },
  {
    challengeId: 2,
    name: "RE Challenge 1",
    description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
    details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
    submisisons: []
  },
  {
    challengeId: 3,
    name: "Crypto Challenge 1",
    description: "Laboris dolore veniam ea ea cillum excepteur proident cillum est. Excepteur id Lorem do quis nostrud non laboris culpa sit et voluptate fugiat amet. Pariatur id proident sint do officia et do ipsum exercitation eiusmod quis. Eiusmod sit nisi elit duis non aliquip non voluptate laboris fugiat consequat occaecat nisi amet. Cillum cillum mollit id ad. Ea fugiat incididunt id incididunt ullamco Lorem.",
    details: "Commodo commodo duis non voluptate sint minim nisi Lorem proident. Deserunt labore consectetur consequat culpa adipisicing dolor velit esse incididunt dolor consectetur sint aliquip elit. Aliquip labore nisi minim mollit mollit nisi adipisicing nostrud dolor tempor do sunt. Tempor nulla occaecat proident dolore occaecat id dolor. Enim sit excepteur eu esse proident ea commodo enim proident enim commodo non.\nCulpa nulla est sint voluptate Lorem occaecat elit voluptate nostrud dolor amet mollit. Irure aliqua pariatur laborum voluptate enim consectetur dolore tempor veniam minim esse amet ad. Et et id pariatur eu commodo in excepteur Lorem aliqua pariatur in anim.\nVeniam elit fugiat mollit sit adipisicing minim aliqua adipisicing esse officia anim ad cupidatat adipisicing. Magna mollit amet nulla officia culpa culpa pariatur nulla in pariatur. Nisi commodo incididunt in laboris labore ad aliqua aute duis commodo. Velit reprehenderit adipisicing aliquip et veniam adipisicing enim id culpa nostrud ea ad dolore. Nisi esse mollit non ex dolor nulla. Consequat reprehenderit culpa Lorem ea id incididunt et nostrud do. Aliqua duis sint aute nisi nulla qui enim.",
    result: "{CTF:CRYPTO1}",
    submisisons: [
      {
        username: "par3",
        isCorrect: true,
      },
      {
        username: "phn210",
        isCorrect: false,
      },
    ]
  }
];

const MOCK_TEAMS = [
  {
    teamId: 0,
    name: "Team CTF 2023 1",
    owner: "hello",
    members: ["hello", "axdaqw", "asdqw"]
  },
  {
    teamId: 1,
    name: "Team CTF 2023 2",
    owner: "axasddaqw",
    members: ["helllllo", "axasddaqw", "asdqwpppp"]
  },
  {
    teamId: 2,
    name: "Team CTF 2023 3",
    owner: "qwpppp",
    members: ["llllo", "axaw", "qwpppp"]
  }
];

const MOCK_USERS = [
  {
    username: "phn2211"
  },
  {
    username: "asdasd"
  },
  {
    username: "qweqwe"
  }
];

const initialState = {
  fetchingAllNewsStatus: QUERY_STATE.INITIAL,
  fetchingAllCTFsStatus: QUERY_STATE.INITIAL,
  fetchingCTFDetailStatus: QUERY_STATE.INITIAL,
  fetchingAllChallengesStatus: QUERY_STATE.INITIAL,
  fetchingChallengeDetailStatus: QUERY_STATE.INITIAL,
  fetchingAllTeamsStatus: QUERY_STATE.INITIAL,
  fetchingPendingCTFsStatus: QUERY_STATE.INITIAL,
  fetchingUsersDataStatus: QUERY_STATE.INITIAL,
  news: [],
  ctfs: [],
  ctfDetail: {},
  challenges: [],
  challengeDetail: {},
  teams: [],
  pendingCtfs: [],
  users: [],
};

export const fetchAllNewsData = () => async (dispatch) => {
  try {
    // FIXME - mock data
    const news = MOCK_NEWS;
    // const news = await defaultClient.get("/news");
    dispatch(fetchAllNewsDataSuccess({ news: news }));
  } catch (error) {
    dispatch(fetchAllNewsDataFail());
    console.error("Fetch news failed:", error);
  }
};

export const fetchAllCTFsData = () => async (dispatch) => {
  try {
    const ctfs = MOCK_CTFS
    // const ctfs = await defaultClient.get("/ctfs");
    console.log(ctfs);
    dispatch(fetchAllCTFsDataSuccess({ ctfs: ctfs }));
  } catch (error) {
    dispatch(fetchAllCTFsDataFail());
    console.error("Fetch CTFs failed:", error);
  }
}

export const fetchCTFDetailData = (ctfId) => async (dispatch) => {
  try {
    const ctfDetail = MOCK_CTFS[ctfId].challenges;
    // const ctfDetail = await defaultClient.get(`/ctfs/${ctfId}`);
    dispatch(fetchCTFDetailSuccess({ ctfDetail: ctfDetail }));
  } catch (error) {
    dispatch(fetchCTFDetailFail());
    console.error("Fetch CTF failed:", error);
  }
}

export const fetchAllChallengesData = () => async (dispatch) => {
  try {
    const challenges = MOCK_CHALLENGES;
    // const challenges = await defaultClient.get("/challenges");
    dispatch(fetchAllChallengesDataSuccess({ challenges: challenges }));
  } catch (error) {
    dispatch(fetchAllChallengesDataFail());
    console.error("Fetch Challenges failed:", error);
  }
}

export const fetchChallengeDetailData = (challengeId) => async (dispatch) => {
  try {
    const challengeDetail = await defaultClient.get(`/challenges/${challengeId}`);
    dispatch(fetchingChallengeDetailSuccess({ challengeDetail: challengeDetail }));
  } catch (error) {
    dispatch(fetchingChallengeDetailFail());
    console.error("Fetch CTF failed:", error);
  }
}

export const fetchAllTeamsData = () => async (dispatch) => {
  try {
    // FIXME - mock data
    const teams = MOCK_TEAMS;
    // const teams = await defaultClient.get("/teams");
    dispatch(fetchingAllTeamsDataSuccess({ teams: teams }));
  } catch (error) {
    dispatch(fetchingAllTeamsDataFail());
    console.error("Fetch Teams failed:", error);
  }
}

export const fetchPendingCTFsData = () => async (dispatch) => {
  try {
    // const pendingCtfs = (await defaultClient.get("/ctfs"));
    const pendingCtfs = MOCK_CTFS;
    dispatch(fetchingAllTeamsDataSuccess({ pendingCtfs: pendingCtfs }));
  } catch (error) {
    dispatch(fetchingAllTeamsDataFail());
    console.error("Fetch pending CTFs failed:", error);
  }
}

export const fetchUsersData = () => async (dispatch) => {
  try {
    // FIXME - mock data
    const users = MOCK_USERS;
    // const users = await defaultClient.get("/users");
    dispatch(fetchingUsersDataSuccess({ users: users }));
  } catch (error) {
    dispatch(fetchingUsersDataFail());
    console.error("Fetch Users failed:", error);
  }
}

const publicDataSlice = createSlice({
  name: "publicDataSlice",
  initialState: initialState,
  reducers: {
    fetchAllNewsDataSuccess: (state, action) => {
      state.news = action.payload.news;
      state.fetchingAllNewsStatus = QUERY_STATE.SUCCESS;
    },
    fetchAllNewsDataFail: (state) => {
      state.fetchingAllNewsStatus = QUERY_STATE.FAIL;
    },
    fetchAllCTFsDataSuccess: (state, action) => {
      state.ctfs = action.payload.ctfs;
      state.fetchingAllCTFsStatus = QUERY_STATE.SUCCESS;
    },
    fetchAllCTFsDataFail: (state) => {
      state.fetchingAllCTFsStatus = QUERY_STATE.FAIL;
    },
    fetchCTFDetailSuccess: (state, action) => {
      state.ctfDetail = action.payload.ctfDetail;
      state.fetchingCTFDetailStatus = QUERY_STATE.SUCCESS;
    },
    fetchCTFDetailFail: (state) => {
      state.fetchingCTFDetailStatus = QUERY_STATE.FAIL;
    },
    fetchAllChallengesDataSuccess: (state, action) => {
      state.challenges = action.payload.challenges;
      state.fetchingAllChallengesStatus = QUERY_STATE.SUCCESS;
    },
    fetchAllChallengesDataFail: (state) => {
      state.fetchingAllChallengesStatus = QUERY_STATE.FAIL;
    },
    fetchingChallengeDetailSuccess: (state, action) => {
      state.challengeDetail = action.payload.challengeDetail;
      state.fetchingChallengeDetailStatus = QUERY_STATE.SUCCESS;
    },
    fetchingChallengeDetailFail: (state) => {
      state.fetchingChallengeDetailStatus = QUERY_STATE.FAIL;
    },
    fetchingAllTeamsDataSuccess: (state, action) => {
      state.teams = action.payload.teams;
      state.fetchingAllTeamsStatus = QUERY_STATE.SUCCESS;
    },
    fetchingAllTeamsDataFail: (state) => {
      state.fetchingAllTeamsStatus = QUERY_STATE.FAIL;
    },
    fetchingPendingCTFsDataSuccess: (state, action) => {
      state.pendingCtfs = action.payload.pendingCtfs;
      state.fetchingPendingCTFsStatus = QUERY_STATE.SUCCESS;
    },
    fetchingPendingCTFsDataFail: (state) => {
      state.fetchingPendingCTFsStatus = QUERY_STATE.FAIL;
    },
    fetchingUsersDataSuccess: (state, action) => {
      state.users = action.payload.users;
      state.fetchingUsersDataStatus = QUERY_STATE.SUCCESS;
    },
    fetchingUsersDataFail: (state) => {
      state.fetchingUsersDataStatus = QUERY_STATE.FAIL;
    }
  }
});

export default publicDataSlice.reducer;
export const {
  fetchAllNewsDataSuccess,
  fetchAllNewsDataFail,
  fetchAllCTFsDataSuccess,
  fetchAllCTFsDataFail,
  fetchCTFDetailSuccess,
  fetchCTFDetailFail,
  fetchAllChallengesDataSuccess,
  fetchAllChallengesDataFail,
  fetchingChallengeDetailSuccess,
  fetchingChallengeDetailFail,
  fetchingAllTeamsDataSuccess,
  fetchingAllTeamsDataFail,
  fetchingUsersDataSuccess,
  fetchingUsersDataFail
} = publicDataSlice.actions;