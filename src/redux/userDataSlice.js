import { createSlice } from "@reduxjs/toolkit";
import { QUERY_STATE, ROLE } from "../configs/constance";
import { defaultClient } from "../services/restClient";

const MOCK_PARTICIPANT = {
  team: {
    teamId: undefined,
    name: "CTF Avenger",
    owner: "phn2211",
    members: ["phn2211", "romain89", "phn210", "bleu210"],
  }
};

const initialState = {
  fetchingUserDataStatus: QUERY_STATE.INITIAL,
  fetchingParticipantTeamStatus: QUERY_STATE.INITIAL,
  fetchingOrganizerCTFsStatus: QUERY_STATE.INITIAL,
  fetchingOrganizerNewsStatus: QUERY_STATE.INITIAL,
  isLogin: false,
  username: "",
  userId: undefined,
  role: ROLE.GUEST,
  participant: {
    team: {
      teamId: undefined,
      name: "",
      owner: "",
      members: [],
    },
  },
  organizer: {
    avatar: "",
    contact: "",
    name: "",
    numViews: undefined,
    ctfs: [],
  },
  discussions: []
};

const storeJwtToken = (token) => {
  localStorage.setItem("jwtToken", token);
}

const loadJwtToken = () => {
  return localStorage.getItem("jwtToken");
}

const clearJwtToken = () => {
  localStorage.removeItem("jwtToken")
}

/* Organizer users actions */

export const fetchParticipantData = async (username) => {
  try {
    const participant = MOCK_PARTICIPANT;
    return { participant };
  } catch (error) {
    console.error("Fetch Participant failed:", error);
    return;
  }
}

export const participateIndividual = (ctfId) => async (dispatch) => {
  try {
    const jwtToken = loadJwtToken();
    await defaultClient.post(`/ctfs/${ctfId}/join`, {}, {
      headers: {
        Authorization: "Bearer " + jwtToken
      }
    });
  } catch (error) {
    console.error("Participate as an individual fail")
  }
}

/* Organizer users actions */

export const fetchOrganizerData = async (userId) => {
  try {
    const { ctfs, organizer } = await defaultClient.get(`/organizers/${userId}`);
    organizer.ctfs = ctfs;
    return organizer;
  } catch (error) {
    console.error("Fetch Organizer failed:", error);
    return;
  }
}

export const fetchOrganizerCTFs = (userId) => async (dispatch) => {
  try {
    const { ctfs, organizer } = await defaultClient.get(`/organizers/${userId}`);
    return ctfs;
  } catch (error) {
    console.error("Fetch Organizer CTFs failed:", error);
    return;
  }
}

export const createCTF = (ctf) => async (dispatch) => {
  try {
    const jwtToken = loadJwtToken();
    ctf.organizerId = 1;

    await defaultClient.post("/ctfs", ctf, {
      headers: {
        Authorization: "Bearer " + jwtToken
      }
    })
  } catch (error) {
    console.error(error);
  }
}

/* Default users actions */

export const fetchUserData = () => async (dispatch) => {
  const user = {
    username: "",
    role: ROLE.GUEST,
  }
  let jwtToken = loadJwtToken();
  if (jwtToken) {
    try {
      const fetchAccount = await defaultClient.get("/auth/verify-jwt", {
        headers: {
          Authorization: "Bearer " + jwtToken
        }
      });
      user.username = fetchAccount.split(',')[0].slice(10);
      user.role = fetchAccount.split(',')[1].slice(5, -1);
      user.isLogin = true;
    } catch (error) {
      user.isLogin = false;
    }
  } else user.isLogin = false;
  if (user.isLogin) {
    try {
      const userData = (await defaultClient.get("/users")).filter(e => e.username === user.username)[0];
      user.userId = userData.userId;
      if (user.role === ROLE.PARTICIPANT) {
        let data = await fetchParticipantData(user.userId);
        user.participant = data.participant;
      }
      else user.participant = initialState.participant;
      if (user.role === ROLE.ORGANIZER) {
        let data = await fetchOrganizerData(user.userId);
        user.organizer = data;
      }
      else user.organizer = initialState.organizer;
      dispatch(fetchUserDataSuccess({ user: user }));
    } catch (error) {
      dispatch(fetchUserDataFail());
      console.error("Fetch User failed:", error);
    }
  }

}

export const signUp = ({ username, email, role, password }) => async (dispatch) => {
  try {
    let signUpDatta = await defaultClient.post("/auth/signup", {
      username: username,
      email: email,
      role: role,
      password: password
    });
    console.log("Sign Up Data:", signUpDatta.token);

    dispatch(signUpSuccess({
      username: username,
      token: signUpDatta.token,
    }));

    dispatch(fetchUserData());
  } catch (error) {
    console.error(error);
  }
}

export const signIn = ({ username, role, password }) => async (dispatch) => {
  try {
    let signInData = await defaultClient.post("/auth/signin", {
      username: username,
      password: password
    });
    console.log("Sign In Data:", signInData.token);

    dispatch(signInSuccess({
      username: username,
      role: role,
      token: signInData.token,
    }));

    dispatch(fetchUserData());
  } catch (error) {
    console.error(error);
  }
}

export const signOut = () => (dispatch) => {
  dispatch(signOutSuccess());
}

const userDataSlice = createSlice({
  name: "userDataSlice",
  initialState: initialState,
  reducers: {
    signUpSuccess: (state, action) => {
      console.log(action);
      state.isLogin = true;
      state.username = action.payload.username;
      state.role = action.payload.role;
      storeJwtToken(action.payload.token);
    },
    signInSuccess: (state, action) => {
      state.isLogin = true;
      state.username = action.payload.username;
      storeJwtToken(action.payload.token);
    },
    signOutSuccess: (state) => {
      Object.assign(state, initialState);
      clearJwtToken();
    },
    fetchUserDataSuccess: (state, action) => {
      state.isLogin = action.payload.user.isLogin;
      state.username = action.payload.user.username;
      state.role = action.payload.user.role;
      state.participant = action.payload.user.participant;
      state.organizer = action.payload.user.organizer;
      state.discussions = action.payload.user.discussions;
      state.fetchingUserDataStatus = QUERY_STATE.SUCCESS;
    },
    fetchUserDataFail: (state) => {
      state.fetchingUserDataStatus = QUERY_STATE.FAIL;
    },
    fetchOrganizerCTFsSuccess: (state, action) => {
      state.organizer.ctfs = action.payload.ctfs;
      state.fetchingOrganizerCTFsStatus = QUERY_STATE.SUCCESS;
    },
    fetchOrganizerCTFsFail: (state) => {
      state.fetchingOrganizerCTFsStatus = QUERY_STATE.FAIL;
    }
  }
});

export default userDataSlice.reducer;
export const {
  fetchUserDataSuccess,
  fetchUserDataFail,
  signUpSuccess,
  signInSuccess,
  signOutSuccess,
  fetchOrganizerCTFsSuccess,
  fetchOrganizerCTFsFail
} = userDataSlice.actions;